#!/usr/bin/python3

import os

dirs = os.listdir('src/dlls')
if len(dirs) == 0:
    print('src/dlls not found, exit')
    exit(0)

whitelist = [
    'ntdll',
    'kernel32',
    'kernelbase',
    'user32',
    'gdi32',
    # add yours
]

def name2gist(name):
    blocked = 'true' if name not in whitelist else 'false'
    return '"src/dlls/{0}": {1},\n'.format(name, blocked)

dirs = map(name2gist, sorted(dirs))

with open('tmp/dll-excludes.txt', 'w') as f:
    f.writelines(dirs)
