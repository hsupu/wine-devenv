# wine dev environment

## preparation

run the following instructions in order:

- `install-deps`
- `init`
- `configure`
- `makeall`

## DirectShow dev

notice: connect your camera to VM.

`makevideo` if source code changes, then `runqq` or `runwx` for debug.

## commit

go subdirectory `src` and commit to `wine-dev` repo, DO NOT commit on root directory (and so `wine-dev-env` repo).
