# FAQ

## Chinese Fonts

1. open `wine regedit`, go `HKLM/Softwares/Microsoft/Windows NT/Current Version/FontSubstitutes`, set `MS Shell Dig` to `simsun`.
2. copy `C:\Windows\Fonts\SIMSUN.TTC` to `~/.wine/driver_c/windows/Fonts/`.

## QQ is broken

open `winedbg` and set:

- disable: `ntoskrnl.exe` `txplatform.exe`
- native first: `riched20` `riched32`
